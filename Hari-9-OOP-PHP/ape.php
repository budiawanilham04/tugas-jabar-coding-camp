<?php

  # Melakukan import class Animal
  require_once 'animal.php';
  
  
  /**
   * Membuat class extends dari class Animal
   */
  class Ape extends Animal
  {
    
    # Membuat property
    public $legs = 2;
  
    # Membuat method
    public function yell()
    {
      return "Auooo";
    }
    
  }

?>
