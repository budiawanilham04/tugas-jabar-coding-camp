<?php

  # Melakukan import class Animal
  require_once 'animal.php';
  
  /**
   * Membuat class extends dari class Animal
   */
  class Frog extends Animal
  {
    
    # Membuat method
    public function jump()
    {
      return "Hop Hop";
    }
    
  }

?>
