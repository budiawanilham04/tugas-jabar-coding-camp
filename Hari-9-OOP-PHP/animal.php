<?php

  /**
   * Membuat class Animal
   */
  class Animal
  {
    
    # Membuat property 
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    
    # Membuat method constructor
    public function __construct($name)
    {
      $this->name = $name;
    }
    
  }

?>
