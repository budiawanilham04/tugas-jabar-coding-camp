<?php
  
  # Melakukan import class 
  require 'animal.php';
  require 'Frog.php';
  require 'Ape.php';
  
  # Membuat object sheep dari class Animal
  $sheep = new Animal("shaun");

  echo "Name : $sheep->name <br>"; // "shaun"
  echo "legs : $sheep->legs <br>"; // 4
  echo "cold blooded : $sheep->cold_blooded <br><br>"; // "no"
  
  // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
  
  # Membuat object kodok dari class Animal
  $kodok = new Frog("buduk");
  
  echo "Name : $kodok->name <br>"; // "buduk"
  echo "legs : $kodok->legs <br>"; // 4
  echo "cold blooded : $kodok->cold_blooded <br>"; // "no"
  echo "Jump : " . $kodok->jump(); // "hop hop"
  echo "<br><br>";
  
  # Membuat object sungokong dari class Animal
  $sungokong = new Ape("kera sakti");
  
  echo "Name : $sungokong->name <br>"; // "kera sakti"
  echo "legs : $sungokong->legs <br>"; // 2
  echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
  echo "Yell : " . $sungokong->yell(); // "Auooo"
  echo "<br><br>";
  
?>
