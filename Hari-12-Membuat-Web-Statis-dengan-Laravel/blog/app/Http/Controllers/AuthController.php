<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
       return view('/form');
    }

    public function sendData(Request $request){
        $namadepan =$request['namadepan'];
        $namabelakang =$request['namabelakang'];
        return view ('/welcome', compact('namadepan','namabelakang'));
    }
}
