@extends('adminlte.master')
@section('judul')
    halaman utama
@endsection
@push('script')
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
        <table id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama cast</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col" style="display: flex">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1 btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection
