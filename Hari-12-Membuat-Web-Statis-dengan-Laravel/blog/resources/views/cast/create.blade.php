@extends('adminlte.master')
@section('judul')
    halaman utama
@endsection
@section('content')
    <div>
    <h2>Tambah Data</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
             <div class="form-group">
                <label for="bio">bio</label>
                <textarea type="text" class="form-control" name="bio" placeholder="Masukkan bio"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
