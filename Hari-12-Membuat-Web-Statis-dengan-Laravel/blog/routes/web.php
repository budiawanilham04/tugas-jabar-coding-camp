<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@sendData');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/table', function(){
    return view('table');
});


Route::get('/data-table', function(){
    return view('data-table');
});

// CRUD QUERY BUILDER
Route::get('/cast', 'cast@index');
Route::get('/cast/create', 'cast@create');
Route::post('/cast', 'cast@store');
Route::get('/cast/{cast_id}', 'cast@show');
Route::get('/cast/{cast_id}/edit', 'cast@edit');
Route::put('/cast/{cast_id}', 'cast@update');
Route::delete('/cast/{cast_id}', 'cast@destroy');
